//
//  SecondViewController.m
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SendViewController.h"

@interface SendViewController ()

@end
@implementation SendViewController

@synthesize mydate;
@synthesize txtDate;
@synthesize txtHours;
@synthesize btnOfDate;
@synthesize btnOfHour;

#define componentCount 1
#define btnDatePressed 0
#define btnHoursPressed 1
#define txtDatePressed 0
#define txtHoursPressed 1

- (void)viewDidLoad
{	
    NSLog(@"tesr");
    datesArr        = [[NSMutableArray alloc]init];
    hoursArr        = [[NSMutableArray alloc]init];
    mydate          = [[NSDate alloc] init];
    NSDate *today   = [[NSDate alloc] init];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    [gregorianCalendar setFirstWeekday:1];
    NSDateComponents *days = [[NSDateComponents alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSInteger dayCount = 0;
    while ( TRUE ) {
        [days setDay: ++dayCount];
        mydate = [gregorianCalendar dateByAddingComponents: days toDate:today options: 0];
        if ( dayCount == 60 )
            break;
        
        BOOL isWeekend = [self checkForWeekend:mydate]; // Any date can be passed here.
        
        if (!isWeekend) {
            [dateFormatter setDateFormat: @"dd MMMM yyyy"];
            NSString *dateDayString = [dateFormatter stringFromDate:mydate];
            [datesArr addObject:dateDayString];
        }
    }
	
    for (int i = 9; i < 22; i++)
	{
        NSString *item = [[NSString alloc] initWithFormat:@"%i:00", i];
        [hoursArr addObject:item];
        item = [[NSString alloc] initWithFormat:@"%i:30", i];
        [hoursArr addObject:item];
	}
    selectedOption = 0;    
    
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return componentCount;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (selectedButton == btnHoursPressed || selectedText == txtHoursPressed) {
        return [hoursArr count];
    }else if (selectedButton == btnDatePressed || selectedText == txtDatePressed) {
        return [datesArr count];
    }
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (selectedButton == btnHoursPressed || selectedText == txtHoursPressed) {
        return (NSString *)[hoursArr objectAtIndex:row];
    }else if (selectedButton == btnDatePressed || selectedText == txtDatePressed) {
        return (NSString *)[datesArr objectAtIndex:row];
    }
    return nil;
}

- (IBAction) showSearchWhereOptions {
    
    // create action sheet
    
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    // create frame for picker view
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    // create picker view
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    
    pickerView.showsSelectionIndicator = YES;

    pickerView.dataSource = self;
    
    pickerView.delegate = self;
    
    // set selected option to what was previously selected
    
    [pickerView selectRow:selectedOption inComponent:0 animated:YES];
    
    // add picker view to action sheet
    
    [actionSheet addSubview:pickerView];
        
    // create close button to hide action sheet
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Close"]];
    
    closeButton.momentary = YES;
    
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    
    closeButton.tintColor = [UIColor blackColor];
    
    // link close button to our dismissActionSheet method
    
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
    
    [actionSheet addSubview:closeButton];
        
    // show action sheet
    
    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
}

- (void) dismissActionSheet {
    
    // hide action sheet
    
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
}

// this method runs whenever the user changes the selected list option

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (selectedButton == btnHoursPressed || selectedText == txtHoursPressed) {
        txtHours.text = [hoursArr objectAtIndex:row];
    }else if (selectedButton == btnDatePressed || selectedText == txtDatePressed) {
        txtDate.text = [datesArr objectAtIndex:row];
    }
        
    selectedOption = row;
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;
}

- (IBAction) buttonClicked:(id) sender
{
    if ([sender tag] == btnDatePressed)
    {
        selectedButton = btnDatePressed;
        selectedText = 0;
    }
    
    if ([sender tag] == btnHoursPressed)
    {
        selectedButton = btnHoursPressed;
        selectedText = 0;
    }
}

- (BOOL)checkForWeekend:(NSDate *)aDate {
    BOOL isWeekendDate = NO;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar setFirstWeekday:1];
        
    NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:aDate];
    NSUInteger weekdayOfDate = [components weekday];
    
    if (weekdayOfDate == 7 || weekdayOfDate == 6) {
        // The date falls somewhere on the first or last days of the week.
        isWeekendDate = YES;
    }
    return isWeekendDate;
}

- (IBAction)getBack:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
    
}

@end
