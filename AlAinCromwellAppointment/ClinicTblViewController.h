//
//  ClinicTblViewController.h
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface ClinicTblViewController : UITableViewController
{
    NSMutableArray *clinicType;
    NSMutableArray *doctors;
    NSMutableArray *doctorsByAnesthesia;
    NSMutableArray *doctorsByDental;
    NSMutableArray *doctorsByDermatology;
    NSMutableArray *doctorsByDietician;
    NSMutableArray *doctorsByENT;
    NSMutableArray *doctorsByGeneralPhysician;
     NSMutableArray *doctorsByGeneralSurgery;
    NSMutableArray *doctorsByInternalMedicin;
    NSMutableArray *doctorsByObstetricsGynaecologist;
    NSMutableArray *doctorsByPediatric;
    NSInteger selectedDoc;
    NSInteger selectedSection;
    NSInteger selectedRow;
}
@property  NSInteger selectedSection;
@property  NSInteger selectedRow;
@property (nonatomic,retain) NSMutableArray *clinicType;
@property (nonatomic,retain) NSMutableArray *doctors;
@property (nonatomic,retain) NSMutableArray *doctorsByAnesthesia;
@property (nonatomic,retain) NSMutableArray *doctorsByDental;
@property (nonatomic,retain) NSMutableArray *doctorsByDermatology;
@property (nonatomic,retain) NSMutableArray *doctorsByDietician;
@property (nonatomic,retain) NSMutableArray *doctorsByENT;
@property (nonatomic,retain) NSMutableArray *doctorsByGeneralPhysician;
@property (nonatomic,retain) NSMutableArray *doctorsByGeneralSurgery;
@property (nonatomic,retain) NSMutableArray *doctorsByInternalMedicin;
@property (nonatomic,retain) NSMutableArray *doctorsByObstetricsGynaecologist;
@property (nonatomic,retain) NSMutableArray *doctorsByPediatric;

- (IBAction) getBack: (id) sender;
@end
