//
//  main.m
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AlAinCromwellAppointmentDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AlAinCromwellAppointmentDelegate class]));
    }
}
