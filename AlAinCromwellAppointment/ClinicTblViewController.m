//
//  ClinicTblViewController.m
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ClinicTblViewController.h"


@interface ClinicTblViewController ()

@end

@implementation ClinicTblViewController
@synthesize clinicType; 
@synthesize doctors; 
@synthesize doctorsByAnesthesia;
@synthesize doctorsByDental;
@synthesize doctorsByDermatology;
@synthesize doctorsByDietician;
@synthesize doctorsByENT;
@synthesize doctorsByGeneralPhysician;
@synthesize doctorsByGeneralSurgery;
@synthesize doctorsByInternalMedicin;
@synthesize doctorsByObstetricsGynaecologist;
@synthesize doctorsByPediatric;
@synthesize selectedRow;
@synthesize selectedSection;
- (void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)loadView{
    [super loadView];
    
    // create a UIButton (Deconnect button)
    UIButton *btnDeco = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnDeco.frame = CGRectMake(10, 47, 100, 37);
    [btnDeco setTitle:@"Back" forState:UIControlStateNormal];
    [btnDeco addTarget:self action:@selector(getBack:) forControlEvents:UIControlEventTouchUpInside];
    
    //Create UINavigation Bar
    //Create UINavigation Bar
    
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    navBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    navBar.tintColor = [UIColor darkGrayColor];
    navBar.alpha = 0.7;
    
    
    //Add Title Item to Navigation Bar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,320,44)];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    label.text = @"AL AIN CROMWELL";
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = UITextAlignmentCenter;
    [navBar addSubview:label];

    
    //create a footer view on the bottom of the tabeview
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 85)];
    [headerView addSubview:navBar];
    [headerView addSubview:btnDeco];
    
    self.tableView.tableHeaderView = headerView; 
}

- (void)viewDidLoad
{    
    [super viewDidLoad];

    clinicType = [[NSMutableArray alloc] init];
        
    [clinicType addObject:@"Anesthesia"];
    [clinicType addObject:@"Dental"];
    [clinicType addObject:@"Dermatology"];
    [clinicType addObject:@"Dietician"];
    [clinicType addObject:@"ENT"];
    [clinicType addObject:@"General Surgery"];
    [clinicType addObject:@"Internal Medicine"];
    [clinicType addObject:@"Obstetrics/Gynaecologist"];
    [clinicType addObject:@"Pediatric"];
    
    doctors = [[NSMutableArray alloc] init];
    doctorsByAnesthesia = [[NSMutableArray alloc] init];
    
    [doctorsByAnesthesia addObject:@"SDr. uma Gopalakrishanan"];
    [doctorsByAnesthesia addObject:@"Dr. Mohammad Moustafa"];
    [doctorsByAnesthesia addObject:@"Dr. Eglal Bazza"];
    
    doctorsByDental = [[NSMutableArray alloc] init];
    [doctorsByDental addObject:@"Dr. Rana El Jaber"];
    
    doctorsByDermatology = [[NSMutableArray alloc] init];
    [doctorsByDermatology addObject:@"Dr. Salma Al Habib"];
    
    doctorsByDietician = [[NSMutableArray alloc] init];
    [doctorsByDietician addObject:@"Dr. Mazahir Abbas Sid"];
    
    doctorsByENT = [[NSMutableArray alloc] init];
    [doctorsByENT addObject:@"Dr. Naser Sarhand"];
    
    doctorsByGeneralPhysician = [[NSMutableArray alloc] init];
    [doctorsByGeneralPhysician addObject:@"Dr. Imen Debbiche"];
    [doctorsByGeneralPhysician addObject:@"Dr. Rasha Hassan"];
    [doctorsByGeneralPhysician addObject:@"Dr. Rabab Saad"];
    [doctorsByGeneralPhysician addObject:@"Dr. Doha Mudather"];
    [doctorsByGeneralPhysician addObject:@"Dr. Eulalie Burger"];
    
    doctorsByGeneralSurgery = [[NSMutableArray alloc] init];
    [doctorsByGeneralSurgery addObject:@"Dr. Mohammad Puttus"];
    
    doctorsByInternalMedicin = [[NSMutableArray alloc] init];
    [doctorsByInternalMedicin addObject:@"Dr. Amjad Al Nuaimy"];
    
    doctorsByObstetricsGynaecologist = [[NSMutableArray alloc] init];
    [doctorsByObstetricsGynaecologist addObject:@"Dr. Alia Adwan"];
    [doctorsByObstetricsGynaecologist addObject:@"Dr. Sabuh Al Omary"];
    [doctorsByObstetricsGynaecologist addObject:@"Dr. Nazek Salman"];
    [doctorsByObstetricsGynaecologist addObject:@"Dr. Faten Ahmad"];
    [doctorsByObstetricsGynaecologist addObject:@"Dr. Fairouz Radi"];
    
    doctorsByPediatric = [[NSMutableArray alloc] init];
    [doctorsByPediatric addObject:@"Dr. Sultan Halloush"];
    [doctorsByPediatric addObject:@"Dr. Jaber Hamaideh"];
    [doctorsByPediatric addObject:@"Dr. Kamal El Tayeb"];
    [doctorsByPediatric addObject:@"Dr. Raja Sekhar"];
    
    
    selectedDoc=-1;
    selectedSection=-1;
}

- (void)viewDidUnload
{
    [super viewDidUnload];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0){
        return @"Clinics";
    }else if(section == 1){
        return @"Doctors";
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(20, 6, 280, 30);
    label.backgroundColor = [UIColor clearColor];
    //label.textColor = [UIColor colorWithHue:(136.0/360.0)  // Slightly bluish green
                                 //saturation:1.0
                                // brightness:0.60
                                      //alpha:1.0];
    label.shadowColor = [UIColor whiteColor];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:16];
    label.text = sectionTitle;
    label.textAlignment = UITextAlignmentRight;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    [view addSubview:label];
    
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        return self.clinicType.count;
    }
    else if(section==1){        
        if(selectedDoc == 0){
            return self.doctorsByAnesthesia.count;
        }else if(selectedDoc == 1){
            return self.doctorsByDental.count;
        }else if(selectedDoc == 2){
            return self.doctorsByDermatology.count;
        }else if(selectedDoc == 3){
            return self.doctorsByDietician.count;
        }else if(selectedDoc == 4){
            return self.doctorsByENT.count;
        }else if(selectedDoc == 5){
            return self.doctorsByGeneralPhysician.count;
        }else if(selectedDoc == 6){
            return self.doctorsByGeneralSurgery.count;
        }else if(selectedDoc== 7){
            return self.doctorsByInternalMedicin.count;
        }else if(selectedDoc == 8){
            return self.doctorsByObstetricsGynaecologist.count;
        }else if(selectedDoc == 9){
            return self.doctorsByPediatric.count;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(indexPath.section==0){ 
        cell.textLabel.text=(NSString *)[clinicType objectAtIndex:indexPath.row];
        
    }
    else if(indexPath.section==1){ 
        cell.textLabel.text=(NSString *)[doctors objectAtIndex:indexPath.row];
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedDoc = indexPath.row;
    self.selectedSection = indexPath.section;
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            doctors= doctorsByAnesthesia;
        }else if(indexPath.row == 1){
            doctors= doctorsByDental;
        }else if(indexPath.row == 2){
            doctors= doctorsByDermatology;
        }else if(indexPath.row == 3){
            doctors= doctorsByDietician;
        }else if(indexPath.row == 4){
            doctors= doctorsByENT;
        }else if(indexPath.row == 5){
            doctors= doctorsByGeneralPhysician;
        }else if(indexPath.row == 6){
            doctors= doctorsByGeneralSurgery;
        }else if(indexPath.row == 7){
            doctors= doctorsByInternalMedicin;
        }else if(indexPath.row == 8){
            doctors= doctorsByObstetricsGynaecologist;
        }else if(indexPath.row == 9){
            doctors= doctorsByPediatric;
        }
        NSIndexSet *xx=[NSIndexSet indexSetWithIndex:1];
        [tableView reloadSections:xx withRowAnimation:UITableViewRowAnimationFade];
    } 
}

- (IBAction)getBack:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
    
}

@end
