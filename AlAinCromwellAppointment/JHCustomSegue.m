//
//  JHCustomSegue.m
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JHCustomSegue.h"

@implementation JHCustomSegue

- (void) perform {
    
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    
    NSIndexPath *indexPath = [clinicTblVController.self.tableView indexPathForSelectedRow];
    if(indexPath.section==1){ 
        [UIView transitionWithView:src.navigationController.view duration:0.2
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            [src.navigationController pushViewController:dst animated:YES];
                        }
                        completion:NULL];
    }
}

@end
