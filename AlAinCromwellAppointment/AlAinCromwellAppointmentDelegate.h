//
//  HelloWorldAppDelegate.h
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlAinCromwellAppointmentDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
