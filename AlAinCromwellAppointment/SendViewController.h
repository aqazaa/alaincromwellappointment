//
//  SecondViewController.h
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    
    NSDate                  *mydate;
    UIActionSheet           *actionSheet;
    NSInteger               selectedOption;
    NSMutableArray          *datesArr;
    NSMutableArray          *hoursArr;
    NSInteger               selectedButton;
    NSInteger               selectedText;
}

@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UITextField *txtHours;

@property (weak, nonatomic) IBOutlet UIButton *btnOfDate;
@property (weak, nonatomic) IBOutlet UIButton *btnOfHour;

@property (nonatomic, retain) NSDate *mydate;
- (IBAction) showSearchWhereOptions;
- (IBAction) buttonClicked:(id) sender;

//- (IBAction) goLastScreen;
- (IBAction) getBack: (id) sender;
@end
