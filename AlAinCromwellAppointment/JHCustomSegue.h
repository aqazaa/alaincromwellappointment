//
//  JHCustomSegue.h
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClinicTblViewController.h"

@interface JHCustomSegue : UIStoryboardSegue
{
    IBOutlet ClinicTblViewController *clinicTblVController;
}
@end
