//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClinicTblViewController.h"

@interface MainViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UIButton *nextButton;
}
@property (weak, nonatomic) IBOutlet UITextField *fname;
@property (weak, nonatomic) IBOutlet UITextField *fthrname;
@property (weak, nonatomic) IBOutlet UITextField *thrdname;
@property (weak, nonatomic) IBOutlet UITextField *lname;
@property (weak, nonatomic) IBOutlet UITextField *keyphoneNo;
@property (weak, nonatomic) IBOutlet UITextField *phoneNo;
@property (weak, nonatomic) IBOutlet UITextField *insuranceCo;
@property (weak, nonatomic) IBOutlet UISwitch *insuredSwtch;

- (IBAction) toggleEnabledForInsuredSwtch: (id) sender;
- (IBAction) validateTextFields;

@end
