//
//  toLastScreen.m
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "toLastScreen.h"
#import "ClinicTblViewController.h"

@implementation toLastScreen
    - (void) perform {            
            ClinicTblViewController *src = (ClinicTblViewController *) self.sourceViewController;
            UIViewController *dst = (UIViewController *) self.destinationViewController;
            if(src.tableView.indexPathForSelectedRow.section == 1)
            {
                [UIView transitionWithView:src.navigationController.view duration:0.2
                               options:UIViewAnimationOptionTransitionFlipFromLeft
                            animations:^{
                                    [src presentModalViewController:dst animated:NO];
                            }
                            completion:NULL]; 
            }
}
@end
