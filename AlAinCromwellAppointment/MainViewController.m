//
//  HelloWorldViewController.m
//  HelloWorld
//
//  Created by Abdullah Ahmed on 3/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"


@interface MainViewController ()

@end
NSInteger offsetView;
@implementation MainViewController

@synthesize fname;
@synthesize fthrname;
@synthesize thrdname;
@synthesize lname;
@synthesize keyphoneNo;
@synthesize phoneNo;
@synthesize insuranceCo;
@synthesize insuredSwtch;

- (IBAction) toggleEnabledForInsuredSwtch: (id) sender {  
    if (insuredSwtch.on) {
        insuranceCo.enabled = YES;
        [insuranceCo setBackgroundColor:[UIColor clearColor]];
    } else { 
        insuranceCo.enabled = NO;
        [insuranceCo setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    }
} 

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad
{
    NSLog(@"Test Commit on GIT");
    offsetView = 0;
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    NSString *keyPhoneNo = keyphoneNo.text;
    NSString *generalFilds = textField.text;
    
    if(generalFilds.length == 0)
    {
        [textField setBackgroundColor:[UIColor redColor]];
        textField.alpha = 0.5;
    }else {
        [textField setBackgroundColor:[UIColor clearColor]];
        textField.alpha = 0.5;
    }
    if (textField == self.keyphoneNo) {
    
        if ( [keyPhoneNo isEqualToString:@"50"] || [keyPhoneNo isEqualToString:@"56"] || [keyPhoneNo isEqualToString:@"55"]) {
            [keyphoneNo setBackgroundColor:[UIColor clearColor]];
            keyphoneNo.alpha = 0.5;
        }else {
            //self.keyphoneNo.text = nil;
            [keyphoneNo setBackgroundColor:[UIColor redColor]];
            keyphoneNo.alpha = 0.5;
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.keyphoneNo) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 2);
    }
    
    if (textField == self.phoneNo) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 7);
    }
        
    return YES;
}






- (void)keyboardWillShow:(NSNotification *)notif
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide:(NSNotification *)notif
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:insuranceCo])
    {
        offsetView = 200;
    }else {
        offsetView = 0;
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard 
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= offsetView;
        rect.size.height += offsetView;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += offsetView;
        rect.size.height -= offsetView;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) 
                                                 name:UIKeyboardWillShowNotification object:self.view.window]; 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil]; 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil]; 
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [fname resignFirstResponder];
    [fthrname resignFirstResponder];
    [thrdname resignFirstResponder];
    [lname resignFirstResponder];
    [keyphoneNo resignFirstResponder];
    [phoneNo resignFirstResponder];
    [insuranceCo resignFirstResponder];
}

-(IBAction) validateTextFields {
    BOOL nameBol = false;
    BOOL keyphoneNoBol = false;
    BOOL phoneNoBol = false;
    
    if ((fname.text.length > 0) && (fthrname.text.length > 0) && (lname.text.length > 0)) {
        nameBol = true;
    }
    else {
        nameBol = false;
    }
    
    if (self.keyphoneNo.text.length > 0 && self.keyphoneNo.text.length == 2) {
        if ( [self.keyphoneNo.text isEqualToString:@"50"] || [self.keyphoneNo.text isEqualToString:@"56"] || [self.keyphoneNo.text isEqualToString:@"55"]) {
            [keyphoneNo setBackgroundColor:[UIColor clearColor]];
            keyphoneNo.alpha = 0.5;
            keyphoneNoBol = true;
        }else {
            [keyphoneNo setBackgroundColor:[UIColor redColor]];
            keyphoneNo.alpha = 0.5;
            keyphoneNoBol = false;
        }
    } else {
        keyphoneNoBol = false;
        [keyphoneNo setBackgroundColor:[UIColor redColor]];
        keyphoneNo.alpha = 0.5;
    }
    
    if (self.phoneNo.text.length > 0 && self.phoneNo.text.length == 7) {
        [keyphoneNo setBackgroundColor:[UIColor clearColor]];
        keyphoneNo.alpha = 0.5;
        phoneNoBol = true;
    } else {
        phoneNoBol = false;
        
        [phoneNo setBackgroundColor:[UIColor redColor]];
        phoneNo.alpha = 0.5;
    }
    
    if (nameBol == true && keyphoneNoBol == true && phoneNoBol == true) {
        nextButton.enabled = YES;
    } else {
        nextButton.enabled = NO;
    }
}

@end
